# Abadonna-overlay

This personal repository contains ebuilds and fixes for packages that I'm interested in.

## Installation

In order to install, please run as root:

```bash
layman -S
layman -a abadonna-overlay
```

In order to verify, please call:
```bash
layman -l
```

## Atoms

### app-backup/copy-agent

Sync, protect, and share your files

Copy offers a simple cloud solution to manage business and personal storage.

For more please visit [https://www.copy.com](https://www.copy.com)

### app-crypt/veracrypt

VeraCrypt is a free disk encryption software that is based on TrueCrypt.

Please note: This atom compiles monolithic VeraCrypt with embedded wxWidgets 3.0.2.

For more please visit [https://veracrypt.codeplex.com/](https://veracrypt.codeplex.com/)

### app-misc/mat

MAT: Metadata Anonymisation Toolkit

MAT is a toolbox composed of a GUI application, a CLI application and a library,
to anonymize/remove metadata.

For more please visit [https://mat.boum.org/](https://mat.boum.org/)

### app-office/Moneydance

Moneydance is easy to use personal finance software that is loaded with all the features you need:
online banking and bill payment, account management, budgeting and investment tracking.
Moneydance handles multiple currencies and virtually any financial task with ease.

For more please visit [http://infinitekind.com/moneydance](http://infinitekind.com/moneydance)

### media-gfx/AfterShotPro

AfterShot Pro is a total photographic workflow for professional & enthusiast photographers.

* Faster! Complete RAW photo-editing workflow
* Enhanced! Advanced non-destructive editing
* NEW! 30% faster with new 64-bit performance

For more please visit [http://www.aftershotpro.com/en/products/aftershot-pro](http://www.aftershotpro.com/en/products/aftershot-pro)

### media-gfx/dispcalGUI

Open Source Display Calibration and Characterization powered by Argyll CMS.

For more please visit [http://dispcalgui.hoech.net/](http://dispcalgui.hoech.net/)

### net-mail/davmail

DavMail POP/IMAP/SMTP/Caldav/Carddav/LDAP Exchange Gateway

The main goal of DavMail is to provide standard compliant protocols in front of proprietary Exchange.
This means LDAP for global address book, SMTP to send messages, IMAP to browse messages on the server in any folder,
POP to retrieve inbox messages only, Caldav for calendar support and Carddav for personal contacts sync.
Thus any standard compliant client can be used with Microsoft Exchange.

For more please visit [http://davmail.sourceforge.net](http://davmail.sourceforge.net)

### net-misc/teamviewer

All-In-One Solution for Remote Access and Support over the Internet.

For more please visit [https://www.teamviewer.com/](https://www.teamviewer.com/)

### www-client/tor-browser

The Tor software protects you by bouncing your communications around a distributed network of relays run by volunteers
all around the world: it prevents somebody watching your Internet connection from learning what sites you visit,
it prevents the sites you visit from learning your physical location, and it lets you access sites which are blocked.

The Tor Browser lets you use Tor on Windows, Mac OS X, or Linux without needing to install any software.

For more please visit [https://www.torproject.org/projects/torbrowser.html.en](https://www.torproject.org/projects/torbrowser.html.en)

## Atoms that are not supported anymore

### app-backup/megasync

MEGA Sync Client: Easy automated syncing between your computer and your MEGA cloud drive.

* ~~SECURE: Your data is encrypted end-to-end~~ [KimDotCom][1],
* FLEXIBLE: Sync any folder from your PC to any folder in the cloud,
* FAST!
* GENEROUS: Store up to 50GB for free.

For more please visit [https://mega.nz](https://mega.nz)

[1]: http://yro.slashdot.org/story/15/07/27/200204/interviews-kim-dotcom-answers-your-questions   "Slashdot"
